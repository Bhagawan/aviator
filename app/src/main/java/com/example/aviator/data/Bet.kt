package com.example.aviator.data

data class Bet(val date: String, val bet: Float, val finalCoefficient: Float, val success: Boolean)
