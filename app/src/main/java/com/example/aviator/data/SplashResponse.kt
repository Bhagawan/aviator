package com.example.aviator.data

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

