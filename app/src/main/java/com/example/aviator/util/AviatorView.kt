package com.example.aviator.util

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.example.aviator.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*


const val OFF_LEFT = 50F
const val OFF_BOTTOM = 50F

class AviatorView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {
    private var mHeight = 0
    private var mWidth = 0
    private var currentAngle = 0.0f
    private var startGrey = false
    private var currX = 0.0f
    private var currY = 0.0f
    private var currInfiniteAngle: Double = 0.0
    private var infiniteMult = 1
    private var plane = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context?.resources, R.drawable.plane), 116, 73, false)
    private val propeller = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context?.resources, R.drawable.propeller), 200, 200, false)

    private var running = true
    private var ending = false
    private var startingPercent = -1.0f

    private var mInterface: AviatorInterface? = null
    private var odd = 1.0f
    private var endOdd = 0.0f


    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            plane = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context?.resources, R.drawable.plane), mWidth / 5, ((mWidth / 5) * (73.0f / 116.0f)).toInt(), false)
        }
    }

    override fun onDraw(canvas: Canvas) {
        canvas.let {
            //checkEnd()
            clipCanvas(it)
            drawField(it)
            if(startingPercent < 5 && startingPercent > 0) showStartScreen(it) else if(startingPercent > 0 && !ending) increaseOdd()
            calculatePlanePosition()
            if(odd > 1.5 && !ending) drawGraph(it, currX.toInt(), currY.toInt())
            drawBorders(it)
            drawPlane(it)
            if(startingPercent > 5 && !ending) drawOdd(it, odd) else if(ending) drawOdd(it, endOdd)
        }
    }

    fun setCallback(callback: AviatorInterface) {
        mInterface = callback
    }

    fun isActive() : Boolean = startingPercent > 5 && !ending

    fun start() {
        ending = false
        currX = 0.0f
        currY = 0.0f
        odd = 1.0f
        startingPercent = 1.0f
    }

    private fun clipCanvas(c: Canvas) {
        val clipPath = Path()
        clipPath.addRoundRect(RectF(c.clipBounds), 35.0f, 35.0f, Path.Direction.CW)
        c.clipPath(clipPath)
    }

    private fun drawField(c: Canvas) {
        c.drawColor(Color.parseColor("#272727"))

        val p = Paint()
        p.style = Paint.Style.FILL
        p.color = Color.BLACK

        val rays = getRays()
        for(i in  0 until (rays.size - 3) step 4) {
            val path = Path()
            path.fillType = Path.FillType.EVEN_ODD
            path.moveTo(0.0f, mHeight.toFloat())
            path.lineTo(rays[i], rays[i + 1])
            if(rays[i + 3] > rays[i + 1]) path.lineTo(mWidth.toFloat(), 0.0f)
            path.lineTo(rays[i + 2], rays[i + 3])
            path.close()
            c.drawPath(path, p)
        }
        if(odd > 1) {
            val red = (7 * odd).toInt().coerceAtMost(255)
            val centerColor = Color.argb(255, red, 0, (255 - red).coerceAtLeast(0))
            val gradient = RadialGradient(mWidth / 2.0f, mHeight / 2.0f, min(mWidth, mHeight) / 2.0f, centerColor, Color.TRANSPARENT, Shader.TileMode.CLAMP)
            p.isDither = true
            p.shader = gradient
            c.drawCircle(mWidth / 2.0f, mHeight / 2.0f, min(mWidth, mHeight) / 2.0f, p)
        }
    }

    private fun drawGraph(c: Canvas, x: Int, y: Int) {
        val pLine = Paint()
        pLine.color = Color.RED
        pLine.strokeWidth = 5.0f

        val pGraph = Paint()
        pGraph.color = Color.parseColor("#80FF0000")

        val floatArray = ArrayList<Float>()
        val a = y / x.toDouble().pow(2)
        for(i in 1..x) {
            floatArray.add(OFF_LEFT + (i - 1).toFloat())
            floatArray.add(mHeight.toFloat() - OFF_BOTTOM - ((i - 1).toDouble().pow(2) * a).toFloat())
            floatArray.add(OFF_LEFT + i.toFloat())
            floatArray.add(mHeight.toFloat() - OFF_BOTTOM - (i.toDouble().pow(2) * a).toFloat())
        }
        c.drawLines(floatArray.toFloatArray(), pLine)

        val path = Path()
        path.fillType = Path.FillType.EVEN_ODD
        path.moveTo(OFF_LEFT, mHeight.toFloat() - OFF_BOTTOM)
        path.lineTo(OFF_LEFT, mHeight.toFloat() - OFF_BOTTOM - (floatArray[0].toDouble().pow(2) * a).toFloat())

        for(i in 0 until floatArray.size / 4) {
            path.lineTo(floatArray[i * 4 + 2], floatArray[i * 4 + 3])
        }
        path.lineTo(floatArray[floatArray.size - 2], mHeight.toFloat() - OFF_BOTTOM )
        path.close()
        c.drawPath(path, pGraph)

    }

    private fun drawBorders(c: Canvas) {
        val p = Paint()
        p.strokeWidth = 3.0f
        p.color = Color.parseColor("#272727")
        c.drawLine(OFF_LEFT, 0.0f, OFF_LEFT, mHeight - OFF_BOTTOM, p)
        c.drawLine(OFF_LEFT, mHeight - OFF_BOTTOM, mWidth.toFloat(), mHeight - OFF_BOTTOM, p)
        val max = if(mWidth > mHeight) mWidth else mHeight

        p.strokeWidth = 5.0f
        for(i in 0..max step 100) {
            if(mHeight - 100 - i > 0) {
                p.color = Color.BLUE
                c.drawCircle(OFF_LEFT / 2, mHeight - 100F - i,4.0f, p)
            }
            if( 100 + i < mWidth) {
                p.color = Color.WHITE
                c.drawCircle(100F + i, mHeight - OFF_BOTTOM / 2, 4.0f, p)
            }
        }
    }

    private fun drawOdd(c: Canvas, number: Float) {
        val p = Paint()
        p.color = Color.WHITE
        p.textAlign = Paint.Align.CENTER
        p.textSize = 140.0f
        p.isFakeBoldText = true

        c.drawText(String.format("%.2f",number) + "x".lowercase(Locale.getDefault()), (mWidth - OFF_LEFT) / 2 + OFF_LEFT, (mHeight - OFF_BOTTOM) / 2 + 70, p)
    }

    private fun drawPlane(c: Canvas) {
        plane.let {
            val p = Paint()
            p.color = Color.RED
            c.drawBitmap(it, currX + OFF_LEFT - it.width / (450 / 92.0f) , mHeight - currY - OFF_BOTTOM - it.height + it.height / (289 / 15.0f), p )
        }
    }

    private fun showStartScreen(c: Canvas) {
        val p  = Paint()
        p.color = Color.WHITE
        p.textAlign = Paint.Align.CENTER
        p.textSize = 40.0f
        p.strokeWidth = 5.0f

        val matrix = Matrix()
        matrix.postRotate(360 * startingPercent / 2)
        val rotated = Bitmap.createBitmap(propeller, 0, 0, propeller.width, propeller.height, matrix, true)
        c.drawBitmap(rotated, (mWidth - rotated.width) / 2.0f, (mHeight - rotated.height) / 2.0f, null)
        c.drawText("Waiting for next round",mWidth  / 2.0f, (mHeight + propeller.height) / 2.0f + 20, p)

        val position = mWidth / 3 / 5 * startingPercent
        val height = (mHeight + propeller.height) / 2.0f + 45 + 20

        c.drawLine((mWidth - mWidth / 3) / 2.0f + position, height, (mWidth + mWidth / 3) / 2.0f, height, p)
        p.color = Color.RED
        c.drawLine((mWidth - mWidth / 3) / 2.0f, height, (mWidth - mWidth / 3) / 2.0f + position, height, p)
        startingPercent += 0.04f
    }

    private fun increaseOdd() {
        odd += 0.03f
        mInterface?.setOdd(odd)
        if(Random().nextInt(10000) < odd) {
            end()
            mInterface?.stop()
        }
    }

    private fun getRays() : List<Float> {
        val rayAngle = 5
        val output = ArrayList<Float>()

        if(!startGrey) output.addAll( elements = listOf(0.0f, 0.0f))

        var i = currentAngle
        while( i <= 90) {
            val x :Float = tan(Math.toRadians(i.toDouble())).toFloat() * mHeight
            val y :Float = mHeight - tan(Math.toRadians(90 - i.toDouble())).toFloat() * mWidth
            if(x <= mWidth) {
                output.add(x)
                output.add(0.0f)
            } else {
                output.add(mWidth.toFloat())
                output.add(y)
            }
            i += rayAngle
        }
        output.addAll( elements = listOf(mWidth.toFloat(), mHeight.toFloat()))

        currentAngle += 0.3f
        if(currentAngle > rayAngle) {
            currentAngle -= rayAngle
            startGrey = !startGrey
        }
        return output
    }

    private fun calculatePlanePosition() {
        if(ending) odd += 1.0f

        if(odd < 30.0f || ending) {
            currX = mWidth / 40.0f * (odd - 1)
            currY = mHeight / 40.0f * (odd - 1)
        } else {
            currInfiniteAngle += 5
            if(currInfiniteAngle > 360) {
                currInfiniteAngle = 0.0
                infiniteMult *= -1
            }
            if(infiniteMult.sign > 0) {
                val dX = sin(Math.toRadians(currInfiniteAngle)) * infiniteMult
                val dY = cos(Math.toRadians(currInfiniteAngle)) * -infiniteMult
                currX += dX.toFloat()
                currY += dY.toFloat()
            } else {
                val dX = sin(Math.toRadians(-currInfiniteAngle)) * -infiniteMult
                val dY = cos(Math.toRadians(-currInfiniteAngle)) * infiniteMult
                currX += dX.toFloat()
                currY += dY.toFloat()
            }
        }
    }

    fun end() {
        ending = true
        endOdd = odd
    }

    interface AviatorInterface {
        fun setOdd(odd: Float)
        fun stop()
    }

}