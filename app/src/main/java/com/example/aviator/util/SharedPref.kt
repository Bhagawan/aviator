package com.example.aviator.util

import android.content.Context
import android.content.SharedPreferences

class SharedPref {
    companion object {

        fun setMoneyAmount(context: Context, amount: Float) {
            val sP: SharedPreferences = context.getSharedPreferences("Money", Context.MODE_PRIVATE)
           sP.edit().putFloat("amount", amount).apply()
        }

        fun getMoney(context : Context) : Float {
            val sp = context.getSharedPreferences("Money", Context.MODE_PRIVATE)
            return sp.getFloat("amount", 100.0f)
        }
    }
}