package com.example.aviator.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.aviator.R

class HistoryAdapter(private val history: ArrayList<Float> = ArrayList()) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {
    private val elementAmount = 10

    init {
        cropHistory()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val oddText: TextView = view.findViewById(R.id.history_item_text)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_history, viewGroup, false)

        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.oddText.text = "%.2f".format(history[position])
        val red = (7 * history[position]).toInt().coerceAtMost(255)
        val color = Color.argb(255, red, 0, (255 - red).coerceAtLeast(0))
        viewHolder.oddText.setTextColor(color)
    }

    override fun getItemCount() = history.size

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newHistory: Array<Float>) {
        history.clear()
        history.addAll(newHistory)
        cropHistory()
        notifyDataSetChanged()
    }

    private fun cropHistory() = history.drop((history.size - elementAmount).coerceAtLeast(0))

}