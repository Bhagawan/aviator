package com.example.aviator.adapters

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.aviator.R
import com.example.aviator.data.Bet

class HistoryBigAdapter() : RecyclerView.Adapter<HistoryBigAdapter.ViewHolder>() {
    private val history: ArrayList<Bet> = ArrayList()


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val layout: LinearLayout = view.findViewById(R.id.layout_history_root)
        val dateText: TextView = view.findViewById(R.id.item_history_big_date)
        val betText: TextView = view.findViewById(R.id.item_history_big_bet)
        val oddText: TextView = view.findViewById(R.id.item_history_big_odd)
        val cashText: TextView = view.findViewById(R.id.item_history_big_cash)
        val imgShield: ImageView = view.findViewById(R.id.item_history_big_img_shield)
        val imgChat: ImageView = view.findViewById(R.id.item_history_big_img_chat)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_history_big, viewGroup, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if(position == 0) {
            viewHolder.layout.setBackgroundColor(Color.TRANSPARENT)

            viewHolder.dateText.setText(R.string.history_tab_date)
            viewHolder.dateText.setTextColor(Color.parseColor("#717171"))

            viewHolder.betText.setText(R.string.history_tab_bet)
            viewHolder.betText.setTextColor(Color.parseColor("#717171"))
            viewHolder.oddText.setText(R.string.history_tab_x)
            viewHolder.oddText.setBackgroundColor(Color.TRANSPARENT)
            viewHolder.oddText.setTextColor(Color.parseColor("#717171"))

            viewHolder.cashText.setText(R.string.history_tab_cash)
            viewHolder.cashText.setTextColor(Color.parseColor("#717171"))

            viewHolder.imgShield.visibility = View.INVISIBLE
            viewHolder.imgChat.visibility = View.INVISIBLE
        } else {
            val bet = history[position - 1]
            if(bet.success) {
                viewHolder.layout.setBackgroundResource(R.drawable.shape_popup)
                viewHolder.layout.backgroundTintList = null
            } else {
                viewHolder.layout.setBackgroundResource(R.drawable.shape_rounded_10)
                viewHolder.layout.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#0E0E0E"))
            }
            if(bet.success) viewHolder.betText.setTextColor(Color.WHITE)
            else  viewHolder.betText.setTextColor(Color.parseColor("#717171"))
            val red = (7 * bet.finalCoefficient).toInt().coerceAtMost(255)
            val color = Color.argb(255, red, 0, (255 - red).coerceAtLeast(0))
            viewHolder.oddText.setTextColor(color)

            viewHolder.dateText.text = bet.date
            viewHolder.betText.text = String.format("%.2f", bet.bet)
            viewHolder.oddText.text = String.format("%.2f", bet.finalCoefficient)
            if(bet.success) viewHolder.cashText.text = String.format("%.2f", bet.bet * bet.finalCoefficient)
            else viewHolder.cashText.text = ""
        }
    }

    override fun getItemCount() = if(history.size > 0) history.size + 1 else 0

    @SuppressLint("NotifyDataSetChanged")
    fun setData(newHistory: List<Bet>) {
        history.clear()
        notifyDataSetChanged()
        history.addAll(newHistory)
        notifyDataSetChanged()
    }
}