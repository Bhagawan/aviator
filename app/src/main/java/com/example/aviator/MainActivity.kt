package com.example.aviator

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.aviator.adapters.HistoryAdapter
import com.example.aviator.adapters.HistoryBigAdapter
import com.example.aviator.data.Bet
import com.example.aviator.databinding.ActivityMainBinding
import com.example.aviator.mvp.MainPresenter
import com.example.aviator.mvp.MainPresenterViewInterface
import com.example.aviator.util.AviatorView
import com.example.aviator.util.SharedPref
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter
import java.util.*
import kotlin.concurrent.timerTask

const val FIRST_BET: Byte = 1
const val SECOND_BET: Byte = 2
const val BET_INACTIVE: Float = -1.0f

class MainActivity : MvpAppCompatActivity() , MainPresenterViewInterface{
    private lateinit var binding : ActivityMainBinding
    private var popupsAmount = 0

    @InjectPresenter
    lateinit var mPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        binding.recyclerHistory.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        binding.recyclerHistory.adapter = HistoryAdapter()
        binding.recyclerHistoryMy.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.recyclerHistoryMy.adapter = HistoryBigAdapter()

        binding.aviatorView.setCallback(object :AviatorView.AviatorInterface {
            override fun setOdd(odd: Float) = mPresenter.setOdd(odd)
            override fun stop() = mPresenter.stop()
        })

        binding.btnBetOne.setOnClickListener {
            if(!binding.aviatorView.isActive()) {
                ViewCompat.setBackgroundTintList(
                    binding.btnBetOne,
                    ContextCompat.getColorStateList(
                        binding.root.context,
                        R.color.red_bright
                    )
                )
                val string = getString(R.string.action_cancel_bet)
                binding.btnBetOne.text = string
                binding.aviatorView.start()
            }
            mPresenter.activateBet(FIRST_BET)
        }
        binding.btnBetTwo.setOnClickListener {
            if(!binding.aviatorView.isActive()) {
                ViewCompat.setBackgroundTintList(
                    binding.btnBetTwo,
                    ContextCompat.getColorStateList(
                        binding.root.context,
                        R.color.red_bright
                    )
                )
                val string = getString(R.string.action_cancel_bet)
                binding.btnBetTwo.text = string
                binding.aviatorView.start()
            }
            mPresenter.activateBet(SECOND_BET)
        }

        binding.buttonAmountOnePanelOne.setOnClickListener {
            mPresenter.increaseBet(FIRST_BET, 1F)
        }
        binding.buttonAmountTwoPanelOne.setOnClickListener {
            mPresenter.increaseBet(FIRST_BET, 2F)
        }
        binding.buttonAmountThreePanelOne.setOnClickListener {
            mPresenter.increaseBet(FIRST_BET, 5F)
        }
        binding.buttonAmountFourPanelOne.setOnClickListener {
            mPresenter.increaseBet(FIRST_BET, 10F)
        }
        binding.buttonAmountOnePanelTwo.setOnClickListener {
            mPresenter.increaseBet(SECOND_BET, 1F)
        }
        binding.buttonAmountTwoPanelTwo.setOnClickListener {
            mPresenter.increaseBet(SECOND_BET, 2F)
        }
        binding.buttonAmountThreePanelTwo.setOnClickListener {
            mPresenter.increaseBet(SECOND_BET, 5F)
        }
        binding.buttonAmountFourPanelTwo.setOnClickListener {
            mPresenter.increaseBet(SECOND_BET, 10F)
        }

        binding.imageButtonBetOneAdd.setOnClickListener {
            mPresenter.increaseBet(FIRST_BET, 0.5f)
        }
        binding.imageButtonBetOneRemove.setOnClickListener {
            mPresenter.increaseBet(FIRST_BET, -0.5f)
        }

        binding.imageButtonBetTwoAdd.setOnClickListener {
            mPresenter.increaseBet(SECOND_BET, 0.5f)
        }
        binding.imageButtonBetTwoRemove.setOnClickListener {
            mPresenter.increaseBet(SECOND_BET, -0.5f)
        }

        binding.switchAutoCashOne.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                if(binding.editTextBetOneAutoAmount.text.toString().isEmpty()) {
                    Toast.makeText(
                        binding.root.context,
                        getString(R.string.error_auto_amount),
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.switchAutoCashOne.isChecked = false
                } else {
                    val amount = binding.editTextBetOneAutoAmount.text.toString().toFloat()
                    if (amount > 1) mPresenter.setAutoBetAmount(FIRST_BET, amount)
                    else {
                        Toast.makeText(
                            binding.root.context,
                            getString(R.string.error_auto_amount),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.switchAutoCashOne.isChecked = false
                    }
                }
            } else mPresenter.setAutoBetAmount(FIRST_BET, -1.0f)
        }
        binding.switchAutoCashTwo.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                if(binding.editTextBetTwoAutoAmount.text.toString().isEmpty()) {
                    Toast.makeText(
                        binding.root.context,
                        getString(R.string.error_auto_amount),
                        Toast.LENGTH_SHORT
                    ).show()
                    binding.switchAutoCashTwo.isChecked = false
                } else {
                    val amount = binding.editTextBetTwoAutoAmount.text.toString().toFloat()
                    if (amount > 1) mPresenter.setAutoBetAmount(SECOND_BET, amount)
                    else {
                        Toast.makeText(
                            binding.root.context,
                            getString(R.string.error_auto_amount),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.switchAutoCashTwo.isChecked = false
                    }
                }
            } else mPresenter.setAutoBetAmount(SECOND_BET, -1.0f)
        }
        binding.imageButtonBetOneAutoClear.setOnClickListener {
            binding.editTextBetOneAutoAmount.text.clear()
            binding.switchAutoCashOne.toggle()
        }
        binding.imageButtonBetTwoAutoClear.setOnClickListener {
            binding.editTextBetTwoAutoAmount.text.clear()
            binding.switchAutoCashTwo.toggle()
        }
        binding.switchAutoBetOne.setOnCheckedChangeListener { _, isChecked ->
            mPresenter.setAutoBet(FIRST_BET, isChecked)
        }
        binding.switchAutoBetTwo.setOnCheckedChangeListener { _, isChecked ->
            mPresenter.setAutoBet(SECOND_BET, isChecked)
        }

        binding.betOneMode.setOnCheckedChangeListener { _, isChecked ->
            when {
                isChecked -> {
                    binding.betOneModeTextManual.setTextColor(ContextCompat.getColor(this,R.color.grey_light_2))
                    binding.betOneModeTextAuto.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.layoutBetOneAuto.visibility = View.VISIBLE
                }
                !isChecked -> {
                    binding.betOneModeTextManual.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.betOneModeTextAuto.setTextColor(ContextCompat.getColor(this,R.color.grey_light_2))
                    binding.layoutBetOneAuto.visibility = View.GONE
                }
            }
        }

        binding.betTwoMode.setOnCheckedChangeListener { _, isChecked ->
            when {
                isChecked -> {
                    binding.betTwoModeTextManual.setTextColor(ContextCompat.getColor(this,R.color.grey_light_2))
                    binding.betTwoModeTextAuto.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.layoutAutoTwo.visibility = View.VISIBLE
                }
                !isChecked -> {
                    binding.betTwoModeTextManual.setTextColor(ContextCompat.getColor(this,R.color.white))
                    binding.betTwoModeTextAuto.setTextColor(ContextCompat.getColor(this,R.color.grey_light_2))
                    binding.layoutAutoTwo.visibility = View.GONE
                }
            }
        }

        binding.editTextBetOneAutoAmount.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) binding.editTextBetOneAutoAmount.clearFocus()
            false
        }
        binding.editTextBetTwoAutoAmount.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) binding.editTextBetTwoAutoAmount.clearFocus()
            false
        }

        setContentView(binding.root)
        mPresenter.initialise(binding.root.context)
    }

    override fun startAviator() = binding.aviatorView.start()

    override fun stopAviator() = binding.aviatorView.end()

    override fun setMoneyAmount(amount: Float) {
        binding.textViewMoneyAmount.text = String.format("%.2f",amount)
        SharedPref.setMoneyAmount(binding.root.context, amount)
    }

    override fun updateHistory(history: Array<Float>) {
        (binding.recyclerHistory.adapter as HistoryAdapter).setData(history)
    }

    override fun updateBetHistory(history: List<Bet>) {
        (binding.recyclerHistoryMy.adapter as HistoryBigAdapter).setData(history)
    }

    override fun setCash(bet: Byte, amount: Float) {
        when(bet) {
            FIRST_BET -> {
                if(amount == BET_INACTIVE) {
                    ViewCompat.setBackgroundTintList(
                        binding.btnBetOne,
                        ContextCompat.getColorStateList(
                            binding.root.context,
                            R.color.green
                        )
                    )
//                    val string = getString(R.string.action_bet) + "\n${String.format("%.2f",amount)}" + getString(R.string.string_currency)
//                    binding.btnBetOne.text = string
                } else {
                    ViewCompat.setBackgroundTintList(
                        binding.btnBetOne,
                        ContextCompat.getColorStateList(
                            binding.root.context,
                            R.color.orange
                        )
                    )
                    val string = getString(R.string.action_cash) + "\n${String.format("%.2f",amount)} " + getString(R.string.string_currency)
                    binding.btnBetOne.text = string
                }
            }
            SECOND_BET -> {
                if(amount == BET_INACTIVE) {
                    ViewCompat.setBackgroundTintList(
                        binding.btnBetTwo,
                        ContextCompat.getColorStateList(
                            binding.root.context,
                            R.color.green
                        )
                    )
//                    val string = getString(R.string.action_bet) + "\n${String.format("%.2f",amount)}" + getString(R.string.string_currency)
//                    binding.btnBetTwo.text = string
                } else {
                    ViewCompat.setBackgroundTintList(
                        binding.btnBetTwo,
                        ContextCompat.getColorStateList(
                            binding.root.context,
                            R.color.orange
                        )
                    )
                    val string = getString(R.string.action_cash) + "\n${String.format("%.2f",amount)} " + getString(R.string.string_currency)
                    binding.btnBetTwo.text = string
                }
            }
        }
    }

    override fun setBetAmount(bet: Byte, amount: Float) {
        when(bet) {
            FIRST_BET -> {
                binding.textBetOne.text = String.format("%.2f",amount)
                val string = getString(R.string.action_bet) + "\n${String.format("%.2f",amount)} " + getString(R.string.string_currency)
                binding.btnBetOne.text = string
            }
            SECOND_BET -> {
                val string = getString(R.string.action_bet) + "\n${String.format("%.2f",amount)} " + getString(R.string.string_currency)
                binding.btnBetTwo.text = string
                binding.textBetTwo.text = String.format("%.2f",amount)
            }
        }
    }

    @SuppressLint("InflateParams")
    override fun defeat() {
        val inflater = layoutInflater
        val popupView = inflater.inflate(R.layout.popout_defeat, null)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = binding.root.height

        val popupWindow = PopupWindow(popupView, width, height, false)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0, 0)

        popupView.setOnClickListener { popupWindow.dismiss() }

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    @SuppressLint("InflateParams")
    override fun showWinPopup(odd: Float, amount: Float) {
        val inflater = layoutInflater
        val popupView = inflater.inflate(R.layout.popup_cashed, null)

        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT

        val popupWindow = PopupWindow(popupView, width, height, false)

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.TOP, 0, (popupsAmount) * 250)

        popupView.setOnClickListener {
            popupWindow.dismiss()
            popupsAmount--
        }
        popupView.findViewById<TextView>(R.id.popup_odd).text = String.format("%.2f",odd)
        popupView.findViewById<TextView>(R.id.popup_cash).text = String.format("%.2f",amount)

        Timer().schedule( timerTask {
            if(popupWindow.isShowing) {
                CoroutineScope(Dispatchers.Main).launch { popupWindow.dismiss() }
                popupsAmount--
            }
        }, 3000L)
        popupsAmount++
    }

}