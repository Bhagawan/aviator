package com.example.aviator.mvp

import android.content.Context
import android.text.format.DateFormat
import com.example.aviator.BET_INACTIVE
import com.example.aviator.FIRST_BET
import com.example.aviator.SECOND_BET
import com.example.aviator.data.Bet
import com.example.aviator.util.SharedPref
import moxy.InjectViewState
import moxy.MvpPresenter
import java.util.*

@InjectViewState
class MainPresenter : MvpPresenter<MainPresenterViewInterface>() {
    private var autoCashOne = -1.0f
    private var autoCashTwo = -1.0f
    private var autoBetOne = false
    private var autoBetTwo = false
    private var firstBetActive = false
    private var secondBetActive = false
    private var money  = 0.0f
    private var betOne = 1.0f
    private var betTwo = 1.0f
    private var odd = 1.0f

    private val oddsHistory = ArrayList<Float>()
    private val betsHistory = ArrayList<Bet>()

    fun initialise(context: Context) {
        money = SharedPref.getMoney(context)
        viewState.setMoneyAmount(money)
        viewState.setBetAmount(FIRST_BET, betOne)
        viewState.setBetAmount(SECOND_BET, betTwo)
    }

    fun setBet(betNumber: Byte, amount: Float) {
        if(amount >= 0) {
            when(betNumber) {
                FIRST_BET -> {
                    val d = amount - betOne
                    if(d <= money) {
                        betOne = amount
                        viewState.setBetAmount(betNumber, amount)
                    }
                }
                SECOND_BET -> {
                    val d = amount - betTwo
                    if(d <= money) {
                        betTwo = amount
                        viewState.setBetAmount(betNumber, amount)
                    }
                }
            }
        }
        viewState.setMoneyAmount(money)
    }

    fun increaseBet(betNumber: Byte, amount: Float) {
        when(betNumber) {
            FIRST_BET -> if(betOne + amount in 0.0f..money) setBet(betNumber, betOne + amount)
            SECOND_BET -> if(betTwo + amount in 0.0f..money) setBet(betNumber, betTwo + amount)
        }
    }

    fun setOdd(odd: Float) {
        this.odd = odd
        if(firstBetActive) viewState.setCash(FIRST_BET, betOne * odd)
        if(secondBetActive) viewState.setCash(SECOND_BET, betTwo * odd)
        if(firstBetActive && autoCashOne > 0 && odd >= autoCashOne) {
            addBet(betOne, odd, true)

            money += betOne * odd
            viewState.showWinPopup(odd,betOne * odd)
            firstBetActive = false
            viewState.setCash(FIRST_BET, BET_INACTIVE)
            viewState.setMoneyAmount(money)
            betOne = betOne.coerceAtMost(money)
            viewState.setBetAmount(FIRST_BET, betOne)
        }
        if(secondBetActive && autoCashTwo > 0 && odd >= autoCashTwo) {
            addBet(betTwo, odd, true)

            money += betTwo * odd
            viewState.showWinPopup(odd,betTwo * odd)
            secondBetActive = false
            viewState.setCash(SECOND_BET, BET_INACTIVE)
            viewState.setMoneyAmount(money)
            betTwo = betTwo.coerceAtMost(money)
            viewState.setBetAmount(SECOND_BET, betTwo)
        }
    }

    fun setAutoBetAmount(betNumber: Byte, amount: Float) {
        when(betNumber) {
            FIRST_BET -> {
                autoCashOne = amount
            }
            SECOND_BET -> {
                autoCashTwo = amount
            }
        }
    }

    fun setAutoBet(betNumber: Byte, state: Boolean) {
        when(betNumber) {
            FIRST_BET -> autoBetOne = state
            SECOND_BET -> autoBetTwo = state
        }
    }

    fun activateBet(betNumber: Byte) {
        when(betNumber) {
            FIRST_BET -> {
                if(firstBetActive) {
                    if(odd > 1) addBet(betOne, odd, true)

                    money += betOne * odd
                    if(odd > 1) viewState.showWinPopup(odd,betOne * odd)
                    firstBetActive = false
                    viewState.setCash(FIRST_BET, BET_INACTIVE)
                    viewState.setMoneyAmount(money)
                    betOne = betOne.coerceAtMost(money)
                    viewState.setBetAmount(FIRST_BET, betOne)
                } else if(odd <= 1.0) {
                    money -= betOne
                    firstBetActive = true
                }
            }
            SECOND_BET -> {
                if(secondBetActive) {
                    if(odd > 1) addBet(betTwo, odd, true)

                    money += betTwo * odd
                    if(odd > 1) viewState.showWinPopup(odd,betTwo * odd)
                    secondBetActive = false
                    viewState.setCash(SECOND_BET, BET_INACTIVE)
                    viewState.setMoneyAmount(money)
                    betTwo = betTwo.coerceAtMost(money)
                    viewState.setBetAmount(SECOND_BET, betTwo)
                } else if(odd <= 1.0) {
                    money -= betTwo
                    secondBetActive = true
                }
            }
        }
        viewState.setMoneyAmount(money)
    }

    fun stop() {
        if(firstBetActive) addBet(betOne, odd, false)
        if(secondBetActive) addBet(betTwo, odd, false)

        oddsHistory.add(odd)
        odd = 1.0f
        if(autoBetOne && money >= betOne) {
            money -= betOne
            viewState.startAviator()
            firstBetActive = true
            viewState.setCash(FIRST_BET, betOne * odd)
        } else {
            firstBetActive = false
            viewState.setCash(FIRST_BET, BET_INACTIVE)
            betOne = betOne.coerceAtMost(money)
            viewState.setBetAmount(FIRST_BET, betOne)
        }

        if(autoBetTwo && money >= betTwo) {
            money -= betTwo
            viewState.startAviator()
            secondBetActive = true
            viewState.setCash(SECOND_BET, betTwo * odd)
        } else {
            secondBetActive = false
            viewState.setCash(SECOND_BET, BET_INACTIVE)
            betTwo = betTwo.coerceAtMost(money)
            viewState.setBetAmount(SECOND_BET, betTwo)
        }

        if(money <= 0.5f) {
            viewState.defeat()
            money += 100
        }
        viewState.updateHistory(oddsHistory.toTypedArray())
        viewState.setMoneyAmount(money)
    }

    private fun addBet(bet: Float, odd: Float, success: Boolean) {
        val time = DateFormat.format("hh:mm", Date()).toString()
        betsHistory.add(Bet(time, bet, odd, success))
        if(betsHistory.size > 10) betsHistory.removeAt(0)
        viewState.updateBetHistory(betsHistory.reversed())
    }
}