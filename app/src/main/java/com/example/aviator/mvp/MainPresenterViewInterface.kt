package com.example.aviator.mvp

import com.example.aviator.data.Bet
import moxy.MvpView
import moxy.viewstate.strategy.alias.AddToEnd
import moxy.viewstate.strategy.alias.OneExecution
import moxy.viewstate.strategy.alias.SingleState

interface MainPresenterViewInterface : MvpView {

    @SingleState
    fun startAviator()

    @SingleState
    fun stopAviator()

    @SingleState
    fun setMoneyAmount(amount: Float)

    @SingleState
    fun updateHistory(history: Array<Float>)

    @SingleState
    fun updateBetHistory(history: List<Bet>)

    @AddToEnd
    fun setCash(bet: Byte, amount: Float)

    @AddToEnd
    fun setBetAmount(bet: Byte, amount: Float)

    @OneExecution
    fun defeat()

    @AddToEnd
    fun showWinPopup(odd: Float, amount: Float)

}